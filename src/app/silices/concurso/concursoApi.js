import axios from 'axios'
import { BACK_URL } from '../../../constants/index'

export const getAllConcursos = (token) => {
    return new Promise((resolve, reject) => {
        return axios({
            url: `${BACK_URL}/api/v1/contests`,
            method: 'GET',
            headers: {
                Authorization: `Bearer ${token}`,
                Accept: 'application/json'
            },
        })
            .then(response => response)
            .then(json => {
                return resolve({
                    data: json.data
                });
            })
            .catch(err => {
                return reject(err.response)
            });
    });
};

export const getConcurso = (token, idUser) => {
    return new Promise((resolve, reject) => {
        return axios({
            url: `${BACK_URL}/api/v1/contests/${idUser}`,
            method: 'GET',
            headers: {
                Authorization: `Bearer ${token}`,
                Accept: 'application/json'
            },
        })
            .then(response => response)
            .then(json => {
                return resolve({
                    data: json.data
                });
            })
            .catch(err => {
                return reject(err.response)
            });
    });
};

export const getConcursosWinners = (token) => {
    return new Promise((resolve, reject) => {
        return axios({
            url: `${BACK_URL}/api/v1/contests/winners`,
            method: 'GET',
            headers: {
                Authorization: `Bearer ${token}`,
                Accept: 'application/json'
            },
        })
            .then(response => response)
            .then(json => {
                return resolve({
                    data: json.data
                });
            })
            .catch(err => {
                return reject(err.response)
            });
    });
};

export const getConcursosOyente = (token, idUsuario) => {
    return new Promise((resolve, reject) => {
        return axios({
            url: `${BACK_URL}/api/v1/contests/my-contests/${idUsuario}`,
            method: 'GET',
            headers: {
                Authorization: `Bearer ${token}`,
                Accept: 'application/json'
            },
        })
            .then(response => response)
            .then(json => {
                return resolve({
                    data: json.data
                });
            })
            .catch(err => {
                return reject(err.response)
            });
    });
};

export const getParticipantesEnConcursos = (token) => {
    return new Promise((resolve, reject) => {
        return axios({
            url: `${BACK_URL}/api/v1/contests/number-participants`,
            method: 'GET',
            headers: {
                Authorization: `Bearer ${token}`,
                Accept: 'application/json'
            },
        })
            .then(response => response)
            .then(json => {
                return resolve({
                    data: json.data
                });
            })
            .catch(err => {
                return reject(err.response)
            });
    });
};

export const postParticipanteConcurso = (body, token) => {
    const data = {
        member_id: body.member_id,
        contest_id: body.contest_id
    } 

    return new Promise((resolve, reject) => {
        return axios({
            url: `${BACK_URL}/api/v1/contests/participant-in-contest`,
            method: 'POST',
            headers: {
                Authorization: `Bearer ${token}`,
                Accept: 'application/json'
            },
            data: data
        })
            .then(response => response)
            .then(json => {
                return resolve({
                    data: json.data
                });
            })
            .catch(err => {
                return reject(err)
            });
    });
};

export const postConcurso = (body, token) => {
    const data = {
        title: body.title,
        description: body.description,
        image: body.image,
        end_date: body.end_date,
        begin_date: body.begin_date,
        advertiser: body.advertiser,
        program: body.program,
        banner_image: body.banner_image,
        aditional_information: body.aditional_information,
        winner_mail: body.winner_mail,
        number_of_winners: body.number_of_winners? body.number_of_winners : 1,
    }

    return new Promise((resolve, reject) => {
        return axios({
            url: `${BACK_URL}/api/v1/contests`,
            method: 'POST',
            headers: {
                Authorization: `Bearer ${token}`,
                Accept: 'application/json'
            },
            data: data
        })
            .then(response => response)
            .then(json => {
                return resolve({
                    data: json.data
                });
            })
            .catch(err => {
                return reject(err)
            });
    });
};

export const postMailWinner = (body, token) => {
    const data = {
        to: body.title,
        subject: body.description,
        body: body.image,
    }

    return new Promise((resolve, reject) => {
        return axios({
            url: `${BACK_URL}/api/v1/email`,
            method: 'POST',
            headers: {
                Authorization: `Bearer ${token}`,
                Accept: 'application/json'
            },
            data: data
        })
            .then(response => response)
            .then(json => {
                return resolve({
                    data: json.data
                });
            })
            .catch(err => {
                return reject(err)
            });
    });
};

export const patchConcurso = (body, token,idConcurso) => {

    const data = {
        title: body.title,
        description: body.description,
        image: body.image,
        end_date: body.end_date,
        begin_date: body.begin_date,
        advertiser: body.advertiser,
        program: body.program,
        banner_image: body.banner_image,
        aditional_information: body.aditional_information,
        contest_state: body.contest_state,
        contest_id: idConcurso,
        winner_mail: body.winner_mail,
        number_of_winners: body.number_of_winners? body.number_of_winners : 1,
    }

    return new Promise((resolve, reject) => {
        return axios({
            url: `${BACK_URL}/api/v1/contests/update/${idConcurso}`,
            method: 'PATCH',
            headers: {
                Authorization: `Bearer ${token}`,
                Accept: 'application/json'
            },
            data: data
        })
            .then(response => response)
            .then(json => {
                return resolve({
                    data: json.data
                });
            })
            .catch(err => {
                return reject(err)
            });
    });
};



export const patchConcursoStars = (body, token) => {

    const data = {
        contest_id: body.idConcurso,
        stars: body.stars
    }

    return new Promise((resolve, reject) => {
        return axios({
            url: `${BACK_URL}/api/v1/contests/stars/`,
            method: 'PATCH',
            headers: {
                Authorization: `Bearer ${token}`,
                Accept: 'application/json'
            },
            data: data
        })
            .then(response => response)
            .then(json => {
                return resolve({
                    data: json.data
                });
            })
            .catch(err => {
                return reject(err)
            });
    });
};

export const patchWinnerConcurso = (idConcurso, token) => {

    return new Promise((resolve, reject) => {
        return axios({
            url: `${BACK_URL}/api/v1/contests/update-winners/${idConcurso}`,
            method: 'PATCH',
            headers: {
                Authorization: `Bearer ${token}`,
                Accept: 'application/json'
            },
        })
            .then(response => response)
            .then(json => {
                return resolve({
                    data: json.data
                });
            })
            .catch(err => {
                return reject(err)
            });
    });
};

export const deleteConcurso = (token, idConcurso) => {
    return new Promise((resolve, reject) => {
        return axios({
            url: `${BACK_URL}/api/v1/contests/${idConcurso}`,
            method: 'DELETE',
            headers: {
                Authorization: `Bearer ${token}`,
                Accept: 'application/json'
            },
        })
            .then(response => response)
            .then(json => {
                return resolve({
                    data: json.data
                });
            })
            .catch(err => {
                return reject(err)
            });
    });
};