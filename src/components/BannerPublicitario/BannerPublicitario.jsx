import React, { useState } from "react";
import "./bannerPublicitario.css";
// Imports
import armandoMedialunas from "../../assets/banners/1000/Armando medialunas_1000.png";
import blinkyBurger from "../../assets/banners/1000/Blinky burger_1000.png";
import bodega1 from "../../assets/banners/1000/Bodega_1_1000.png";
import bodega2 from "../../assets/banners/1000/Bodega_2_1000.png";
import bodega3 from "../../assets/banners/1000/Bodega_3_1000.png";
import cabanaElDiamante from "../../assets/banners/1000/Cabaña El Diamante_1000.png";
import churreriaElArtesano from "../../assets/banners/1000/Churrería El Artesano_1000.png";
import cinepolis from "../../assets/banners/1000/Cinepolis_1000.png";
import dulceMorena from "../../assets/banners/1000/Dulce Morena_1000.png";
import eduardoCoiffeur from "../../assets/banners/1000/Eduardo Coiffeur_1000.png";
import elNinoFeliz from "../../assets/banners/1000/El niño feliz_1000.png";
import esteticaIlianaKlein from "../../assets/banners/1000/Estetica Iliana Klein_1000.png";
import hotelElNevado from "../../assets/banners/1000/Hotel El Nevado_1000.png";
import jmBikes from "../../assets/banners/1000/JM Bikes_1000.png";
import leUthe from "../../assets/banners/1000/Le Utthe_1000.png";
import martinRepuestos from "../../assets/banners/1000/Martin Respuestos_1000.png";
import miguitaDePan from "../../assets/banners/1000/Miguita de pan_1000.png";
import mood from "../../assets/banners/1000/Mood_1000.png";
import mueblesDias from "../../assets/banners/1000/Muebles Días_1000.png";
import naser from "../../assets/banners/1000/Naser_1000.png";
import opticaLabordaNeuquen from "../../assets/banners/1000/Optica Laborda Neuquen_1000.png";
import opticaLabordaPlottier from "../../assets/banners/1000/Optica Laborda Plottier_1000.png";
import opticaSenso from "../../assets/banners/1000/Optica Senso_1000.png";
import parrillaRanchoGrande from "../../assets/banners/1000/Parrilla Rancho Grande_1000.png";
import sushiMak from "../../assets/banners/1000/sushi mak_1000.png";
import fedorco from "../../assets/banners/1000/Fedorco_1000.png";

const imagesMapping = {
  1: [
    { image: sushiMak, url: "https://www.sushimak.com.ar/" },
    {
      image: parrillaRanchoGrande,
      url: "https://www.instagram.com/ranchograndenqn/",
    },
    { image: opticaSenso, url: "https://www.opticasenso.com/" },
    { image: naser, url: "https://www.eventosnaser.com.ar/" },
    { image: bodega1, url: "https://www.familiaschroeder.com/" },
  ], // Monday
  2: [
    { image: mueblesDias, url: "https://www.facebook.com/MUEBLESDIAZNEUQUEN/" },
    { image: mood, url: "https://moodlive.com.ar/" },
    {
      image: miguitaDePan,
      url: "https://www.facebook.com/p/Miguitas-de-pan-100066324855284/",
    },
    {
      image: martinRepuestos,
      url: "https://www.instagram.com/martinrepuestoscentenario/",
    },
    { image: jmBikes, url: "https://www.instagram.com/jmbikesnqn/" },
  ], // Tuesday
  3: [
    {
      image: eduardoCoiffeur,
      url: "https://www.instagram.com/eduardo.coiffeur/?hl=es",
    },
    { image: dulceMorena, url: "https://www.dulcemorena.com.ar/" },
    { image: cinepolis, url: "https://www.cinepolis.com.ar/" },
    {
      image: churreriaElArtesano,
      url: "https://www.instagram.com/el_artesanochurreria/",
    },
    { image: bodega3, url: "https://tienda.familiaschroeder.com/" },
  ], // Wednesday
  4: [
    { image: blinkyBurger, url: "https://www.instagram.com/blinkyburger/" },
    {
      image: armandoMedialunas,
      url: "https://www.instagram.com/armandomedialunas/?hl=es-la",
    },
    { image: bodega2, url: "https://tienda.familiaschroeder.com/" },
    {
      image: opticaLabordaPlottier,
      url: "https://opticalaborda.mitiendanube.com/",
    },
    { image: leUthe, url: "https://www.leutthe.com/" },
  ], // Thursday
  5: [
    { image: hotelElNevado, url: "https://www.hotelnevado.com.ar/" },
    { image: cabanaElDiamante, url: "https://www.cabanasdeldiamante.com/" },
    { image: elNinoFeliz, url: "https://www.instagram.com/elninofeliznqn/" },
    { image: fedorco, url: "https://www.fedorco.com/" },
    {
      image: opticaLabordaNeuquen,
      url: "https://www.instagram.com/opticalabordanqn/",
    },
  ], // Friday
  6: [], // Saturday
  7: [], // Sunday
};

const BannerPublicitario = ({ adIndex }) => {
  const dayOfWeek = new Date().getDay();
  const adsForToday = imagesMapping[dayOfWeek] || [];

  if (
    !adsForToday.length ||
    typeof adIndex === "undefined" ||
    adIndex > adsForToday.length - 1
  ) {
    return null;
  }

  const ad = adsForToday[adIndex];

  return (
    <a href={ad.url} target="_blank" rel="noopener noreferrer">
      <img src={ad.image} className="ad-image" alt="AD" />
    </a>
  );
};

export default BannerPublicitario;
