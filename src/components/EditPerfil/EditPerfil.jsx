/* eslint-disable react-hooks/exhaustive-deps */
import React, { useState, useEffect } from 'react';
import './editPerfil.css'
import Input from '../Input/Input';
import Select from '../Select/Select';
import Boton from '../Boton/Boton'
import NavigateBeforeIcon from '@mui/icons-material/NavigateBefore';
import { useSelector, useDispatch } from 'react-redux';
import { capitalizeFirstLetter } from '../../utils/functions';
import { putUserAsync } from '../../app/silices/usuarios/usuarioThunk';
import { setProfileuUsuario, setStatusMessage } from '../../app/silices/usuarios/usuarioSlice';
import { Audio } from 'react-loader-spinner'
import { Link } from "react-router-dom";
import LogoutIcon from "@mui/icons-material/Logout";
import { setRefreshState } from '../../app/silices/login/loginSlice';
import { redirectToNewPage } from '../../utils/functions';
import { setRefreshStateGoogle } from '../../app/silices/usuarios/usuarioGoogleSlice';
import { setRefreshStateUser } from '../../app/silices/usuarios/usuarioSlice';
import { setRefreshStateBeneficio } from '../../app/silices/beneficio/beneficioSlice';
import { setRefreshStateConcursos } from '../../app/silices/concurso/concursoSlice';
import { DatePicker } from '@mui/x-date-pickers/DatePicker';
const EditPerfil = (props) => {
    const { setPerfil, profile, login, statusMessage } = props
    const dispatch = useDispatch()
    const [isScreenWidth600, setIsScreenWidth600] = useState(false);
    const { profileGoogle } = useSelector(state => state.usuarioGoogleSlice)
    const { loading } = useSelector(state => state.usuarioSlice)
    const { localidadSeleccionada, ocupacionSeleccionada, provinciaSeleccionada, generoSeleccionado } = useSelector(state => state.registroSlice)
    const [dataPass, setDataPass] = useState({ pass1: '', pass2: '' })
    const [passLength, setPassLength] = useState(true)
    const [validPass, setValidPass] = useState(true)
    const [validMail, setValidMail] = useState(true)
    const [validate, setValidate] = useState()

    useEffect(() => {
        const handleResize = () => {
            const { innerWidth: width } = window;
            setIsScreenWidth600(width <= 600);
        };

        window.addEventListener("resize", handleResize);
        handleResize();

        return () => window.removeEventListener("resize", handleResize);
    }, []);

    useEffect(() => {
        if (statusMessage === 'updateFulfilled') {
            setValidate(true)

            setTimeout(() => {
                dispatch(setStatusMessage(''))
                setValidate()
                setPerfil(false)
            }, 2000)
        }

        if (statusMessage === 'updateRejected') {
            setValidate(false)

            setTimeout(() => {
                dispatch(setStatusMessage(''))
                setValidate()
            }, 3000)
        }
    }, [statusMessage])

    useEffect(() => {
        if (dataPass.pass1 === dataPass.pass2 && dataPass.pass1 !== '' && dataPass.pass2 !== '') {
            dispatch(setProfileuUsuario({ password: dataPass.pass1 }))
        }
    }, [dataPass.pass1, dataPass.pass2])

    const handleFullName = (event) => {
        dispatch(setProfileuUsuario({ fullName: event.target.value }))
    }

    const handleEmail = (event) => {
        if (event.target.value.includes("@") && event.target.value.includes(".")) {
            setValidMail(true)
        } else {
            setValidMail(false)
        }

        dispatch(setProfileuUsuario({ email: event.target.value }))
    }

    const handlePassword = (event) => {
        setDataPass({ ...dataPass, pass1: event.target.value })

        if (event.target.value !== dataPass.pass2) {
            setValidPass(false)
        } else {
            setValidPass(true)
        }

        if (event.target.value.length < 6) {
            setPassLength(false)
        } else {
            setPassLength(true)
        }
    }

    const handlePassword2 = (event) => {
        setDataPass({ ...dataPass, pass2: event.target.value })

        if (dataPass.pass1 !== event.target.value) {
            setValidPass(false)
        } else {
            setValidPass(true)
        }

        if (event.target.value.length < 6) {
            setPassLength(false)
        } else {
            setPassLength(true)
        }
    }

    const handleBirthDay = (newValue) => {
        if(newValue != null){
            let formattedDate = `${newValue.getFullYear()}-${('0' + (newValue.getMonth()+1)).slice(-2)}-${('0' + newValue.getDate()).slice(-2)}T12:00:00`;
            dispatch(setProfileuUsuario({ birthDay:formattedDate }))
    }
    }

    const handlePhoneNumber = (event) => {
        if (event.target.value.length <= 13 && event.target.value !== 'e' && event.target.value !== 'E') {
            dispatch(setProfileuUsuario({ phoneNumber: event.target.value }))
        }
    }

    const handleDni = (event) => {
        if (event.target.value.length <= 8 && event.target.value !== 'e' && event.target.value !== 'E') {
            dispatch(setProfileuUsuario({ dni: event.target.value }))
        }
    }

    const handleSaveChange = () => {
        const arrayName = profile.fullName.split(' ')
        const transformName = arrayName.map((element) =>
            capitalizeFirstLetter(element)
        )

        const body = {
            full_name: transformName.join(' '),
            email: profile.email,
            password: profile.password,
            birthDay: new Date(profile.birthDay).toISOString(),
            profession: ocupacionSeleccionada === '' ? profile.profession : ocupacionSeleccionada,
            phone_number: profile.phoneNumber,
            dni: profile.dni,
            genre: generoSeleccionado === '' ? profile.genre : generoSeleccionado,
            city: localidadSeleccionada === '' ? profile.city : localidadSeleccionada,
            province: provinciaSeleccionada === '' ? profile.province : provinciaSeleccionada,
            profile_image: profile.profileImage,
        }

        if (passLength && validPass) {
            dispatch(putUserAsync({ token: login.token, idUser: login.id, body }))
        }
    }

    const handleLogout = () => {
        setPerfil(false)
        dispatch(setRefreshState())
        dispatch(setRefreshStateGoogle())
        dispatch(setRefreshStateUser())
        dispatch(setRefreshStateBeneficio())
        dispatch(setRefreshStateConcursos())
        redirectToNewPage('/')
    }

    return (
        <div className='container-perfil-oyente'>
            <form className='container-box-perfil-oyente'>
                <div className='box-inputs-perfil-oyente'>
                    {
                        profileGoogle.idGoogle === '' &&
                        <>
                            <div className='box-registro-conerror'>
                                <Input type={'text'} placeholder={'Nombre y apellido'} value={profile.fullName} width={isScreenWidth600 ? '' : 2} onChange={handleFullName} color />
                                {
                                    profile.fullName.length === 0 && <span className='span-error-registro'>¡Dato incompleto!</span>
                                }
                            </div>
                            <div className='box-registro-conerror'>
                                <Input type={'email'} placeholder={'Email'} value={profile.email} width={isScreenWidth600 ? '' : 2} onChange={handleEmail} color />
                                {
                                    profile.email.length === 0 && <span className='span-error-registro'>¡Dato incompleto!</span>
                                }
                                {
                                    validMail === false && <span className='span-error-registro span-error-registro-psw'>¡El email tiene un formato incorrecto!</span>
                                }
                            </div>
                        </>
                    }
                    <div className='box-registro-conerror'>
                        
                        <DatePicker  format='dd/MM/yyyy' placeholder={"Fecha de nacimiento"} className='date-picker' value={profile.birthDay.length>10?new Date(profile.birthDay):new Date(profile.birthDay+'T12:00:00')} 
                        onChange={handleBirthDay} />
                        {
                            profile.birthDay.length === 0 && <span className='span-error-registro'>¡Dato incompleto!</span>
                        }
                    </div>
                    <div className='box-registro-conerror'>
                        <Input type={'number'} placeholder={'D.N.I'} value={profile.dni} onChange={handleDni} color />
                        {
                            profile.dni.length === 0 && <span className='span-error-registro'>¡Dato incompleto!</span>
                        }
                    </div>
                    <div className='box-registro-conerror'>
                        <Input type={'number'} placeholder={'Teléfono'} value={profile.phoneNumber} onChange={handlePhoneNumber} color />
                        {
                            profile.phoneNumber.length === 0 && <span className='span-error-registro'>¡Dato incompleto!</span>
                        }
                    </div>
                    <div className='box-registro-conerror'>
                        <Select opciones={'genero'} genero={profile.genre} color />
                        {
                            profile.genre.length === 0 && <span className='span-error-registro'>¡Dato incompleto!</span>
                        }
                    </div>
                    <div className='box-registro-conerror'>
                        <Select opciones={'provincia'} provincia={profile.province} color />
                        {
                            profile.province.length === 0 && <span className='span-error-registro'>¡Dato incompleto!</span>
                        }
                    </div>
                    <div className='box-registro-conerror'>
                        <Select opciones={'localidad'} localidad={profile.city} color />
                        {
                            profile.city.length === 0 && <span className='span-error-registro'>¡Dato incompleto!</span>
                        }
                    </div>
                    <div className='box-registro-conerror'>
                        <Select opciones={'ocupacion'} ocupacion={profile.profession} color />
                        {
                            profile.profession.length === 0 && <span className='span-error-registro'>¡Dato incompleto!</span>
                        }
                    </div>
                </div>

                {
                    profileGoogle.idGoogle === '' &&
                    <>
                        <p className='texto-perfil-oyente'>Modificar contraseña</p>
                        <div className='box-inputs-perfil-oyente'>
                            <div className='box-registro-conerror'>
                                <Input type={'password'} placeholder={'Nueva contraseña'} color onChange={handlePassword} />
                                {
                                    (dataPass.pass1 !== dataPass.pass2) && <span className='span-error-registro span-error-registro-psw'>¡Las contraseñas no coinciden!</span>
                                }
                                {
                                    (dataPass.pass1.length < 6 && dataPass.pass1.length > 1 ) && <span className='span-error-registro span-error-registro-psw'>¡La contraseña debe tener más de 6 caracteres!</span>
                                }
                            </div>
                            <div className='box-registro-conerror'>
                                <Input type={'password'} placeholder={'Confirmar contraseña'} color onChange={handlePassword2} />
                                {
                                    (dataPass.pass1 !== dataPass.pass2) && <span className='span-error-registro span-error-registro-psw'>¡Las contraseñas no coinciden!</span>
                                }
                                {
                                    (dataPass.pass2.length < 6 && dataPass.pass2.length > 1) && <span className='span-error-registro span-error-registro-psw'>¡La contraseña debe tener más de 6 caracteres!</span>
                                }
                            </div>
                        </div>
                    </>
                }

                <div className='botones-perfil-oyente'>
                    {
                        validate === true && <span className='span-ok-registro'>¡Se guardaron los cambios correctamente!</span>
                    }
                    {
                        validate === false && <span className='span-error-registro'>¡No se guardaron los cambios!</span>
                    }

                    {
                        loading ?
                            <div className='loader-all'>
                                <Audio
                                    height="80"
                                    width="80"
                                    radius="9"
                                    color="red"
                                    ariaLabel="Cargando..."
                                />
                                <p>Cargando...</p>
                            </div>
                            :
                            <Boton type={2} text={'Confirmar cambios'} onClick={handleSaveChange} />
                    }
                    {
                        !isScreenWidth600 &&
                        <div className='box-volver-perfil box-volver-perfil-oyente' onClick={() => setPerfil(false)}>
                            <NavigateBeforeIcon sx={{ fontSize: '17px' }} />
                            <p className='texto-volver-perfil'>
                                Volver a la página principal
                            </p>
                        </div>
                    }
                </div>
            </form>

            {
                isScreenWidth600 &&
                <Link to={'/'} className="opcion-nav-oyente opcion-nav-oyente-cerrasesion" onClick={handleLogout}>
                    <LogoutIcon sx={{ fontSize: '18px' }} />
                    Cerrar sesión
                </Link>
            }
        </div>
    );
};

export default EditPerfil;